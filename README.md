# The Varehandel Project

## In collaboration with CC Gjøvik


[Check the wiki for detailed information around this project.](https://bitbucket.org/HenrikSolum/varehandel-project/wiki/Home)

## Developers

* Henrik Haugom Solum
* Jostein Enes
* Haakon Reiss-Jacobsen


## Summary

The goal of this project is to create a shopping platform online for local entrepreneurs, so that they can compete against the big retailers by staying up to date on the technology front. In this project we've made a prototype for CC Gjøvik that are going to be used to entice the local entrepreneurs to enter the world of online shopping. CC Gjøvik believes that it's crucial for a business today to be online, to survive against the big chain stores. 