package ntnu.no.varehandel;

public class Shop {
    private int id;
    private String name;
    private String productPrice;
    private String imageURL;
    private int productQuantity;
    private String type;

    /**
     * Initializing of the object with the needed information.
     * @param id The id of the shop.
     * @param name The name of the shop.
     * @param type The type of shop
     * @param imageURL The path to where the image for this product is located.
     */
    public Shop(int id, String name, String productPrice, String imageURL, String type) {
        this.id = id;
        this.name = name;
        this.productPrice = productPrice;
        this.type = type;
        this.imageURL = imageURL;
    }

    /**
     * Initializing of the object with the needed information.
     * @param name The name of the shop
     * @param imageURL url for the logo of the shop
     * @param productPrice price of the current product
     * @param productQuantity amount of the products in store
     */
    public Shop(String name, String imageURL, String productPrice, int productQuantity, int shopId) {
        this.name = name;
        this.productPrice = productPrice;
        this.imageURL = imageURL;
        this.productQuantity = productQuantity;
        this.id = shopId;
    }

    /**
     * Initializes a project object with an id, name and imageURL
     * @param id the id of this Shop object.
     * @param name the name of this Shop object.
     * @param imgURL The imageURL of this object.
     */
    public Shop(int id, String name, String imgURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imgURL;
    }

    /**
     * Initializing of the object with the needed information.
     * @param name The name of the shop.
     * @param imgURL The path to where the image for this product is located.
     */
    public Shop(String name, String imgURL){
        this.name = name;
        this.imageURL = imgURL;
    }

    /**
     * Returns the id of the shop
     * @return id The shop id.
     */
    public int getId() {
        return id;
    }

    /**
     * gets the product price
     * @return price of current product
     */
    public String getProductPrice() { return productPrice;}

    /**
     * Gets the name of the shop.
     * @return name The name of the shop.
     */
    public String getName() {
        return name;
    }


    /**
     * Gets the type of the shop.
     * @return the type of the shop.
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the image location for a given shop.
     * @return The URL to the image for this shop.
     */
    public String getImageURL() {
        return imageURL;
    }

    public int getProductQuantity() { return productQuantity;}
}
