package ntnu.no.varehandel;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ShopActivity extends TemplateActivity implements View.OnClickListener {
    private String id;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getIntent().getStringExtra("name"));
        setContentView(R.layout.activity_shop);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.shop_toolbar);
        setSupportActionBar(myToolbar);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.shop_toolbar).setElevation(4);
        }

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        ImageView image = (ImageView) findViewById(R.id.shopPicture);

        new ImageDownloader(image).execute(getIntent().getStringExtra("imageURL"));

        id = getIntent().getStringExtra("id");

        // Initiates the buttons in the activity
        Button productListViewBtn = (Button) findViewById(R.id.productsBtn);
        Button favoritesBtn = (Button) findViewById(R.id.favoritesBtn);
        Button infoBtn = (Button) findViewById(R.id.infoBtn);
        Button locationBtn = (Button) findViewById(R.id.locationBtn);


        // Adding listeners to the buttons
        productListViewBtn.setOnClickListener(this);
        favoritesBtn.setOnClickListener(this);
        infoBtn.setOnClickListener(this);
        locationBtn.setOnClickListener(this);
    }


    /**
     * When something clickable has been clicked. Decides what to do based on what view was clicked.
     * @param v The view that was clicked(example: Button).
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.productsBtn:
                Intent productsIntent = new Intent(getBaseContext(), ProductListViewActivity.class);
                productsIntent.putExtra("scriptUrl", "http://folk.ntnu.no/henrihso/getShopProducts.php?shopId=" + id);
                productsIntent.putExtra("title", getString(R.string.products));
                startActivity(productsIntent);
                break;

            case R.id.favoritesBtn:
                Toast.makeText(getBaseContext(), R.string.not_implemented, Toast.LENGTH_SHORT).show();
                break;

            case R.id.infoBtn:
                Toast.makeText(getBaseContext(), R.string.not_implemented, Toast.LENGTH_SHORT).show();
                break;

            case R.id.locationBtn:
                Toast.makeText(getBaseContext(), R.string.not_implemented, Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }
}
