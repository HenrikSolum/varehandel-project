package ntnu.no.varehandel;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Class to display a single product in a more detailed view.
 *
 * @author Henrik Haugom Solum, Haakon Reiss-Jacbosen
 */
public class ProductActivity extends TemplateActivity implements ScriptResponse, View.OnClickListener {
    private ScriptExecutor scriptExecutor;

    private ArrayList<Product> productList;
    private ArrayList<Shop> shopList = new ArrayList<>();
    private ImageView productImage;

    private TextView productName;
    private TextView shopName;
    private TextView quantityInShop;
    private TextView productPrice;

    private int currentPrice; //Price of the current selected shop
    private int position;
    private int productId;
    private int shopId;

    private Spinner description;
    private Spinner selectShop;
    private EditText quantity;

    /**
     * @param savedInstanceState saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        // Specifying the toolbar layout
        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);

        // Enable the Up button
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.main_toolbar).setElevation(4);
        }

        // Initialize buttons and the editable field for the quantity.
        Button increaseButton = (Button) findViewById(R.id.increaseQuantityButton);
        Button decreaseButton = (Button) findViewById(R.id.decreaseQuantityButton);
        Button addToBasket = (Button) findViewById(R.id.add_to_basket);
        quantity = (EditText) findViewById(R.id.quantity);

        // Set onclick listeners on the increase and decrease buttons.

        increaseButton.setOnClickListener(this);
        decreaseButton.setOnClickListener(this);
        addToBasket.setOnClickListener(this);


        // Get the id of the product from the intent and launch a script with the ID as paramter.
        String productID = getIntent().getStringExtra("id");

        quantityInShop = (TextView) findViewById(R.id.shop_quantity);
        productPrice = (TextView) findViewById(R.id.productPrice);
        shopName = (TextView) findViewById(R.id.name_of_shop);


        // Check if an id was actually sent to this activity.
        if (productID != null) {
            scriptExecutor = new ScriptExecutor("http://folk.ntnu.no/henrihso/product_in_shops.php?id=" + productID);


            // Get a reference to the callback response.
            scriptExecutor.scriptResponse = this;

            // Run the scriptExecutor.
            scriptExecutor.execute();

        } else productName.setText(getString(R.string.product_notFound));
    }

    /**
     * Callback from the ScriptExecutor. The String s is the JSON that was returned from
     * the php script.  Parses the string (s( as a JSON objects and arrays and fills the view with
     * all the results.
     *
     * @param s The json string to be parsed.
     */
    @Override
    public void finished(String s) {
        try {
            // Handling the JSON object from our API.
            JSONObject json = new JSONObject(s);
            Log.i("json", s);
            if (!(json.getInt("code") == 404) || json.getInt("code") == 200) {

                JSONObject product = json.getJSONObject("product");

                final ImageView productImage = (ImageView) findViewById(R.id.productImage);

                setTitle(product.getString("productName"));

                new ImageDownloader(productImage).execute(product.getString("productPicture"));

                //get and set product description for text view
                TextView descriptionText = (TextView) findViewById(R.id.description);
                descriptionText.setText(product.getString("productDescription"));

                //gets id of product
                productId = product.getInt("productId");

                // Get all the information from the JSON and add them to a Product object.
                JSONArray shops = json.getJSONObject("shops").getJSONArray("shops");
                int amount = shops.length();

                for (int i = 0; i < amount; i++) {
                    shopList.add(new Shop(
                            shops.getJSONObject(i).getString("shopName"),
                            shops.getJSONObject(i).getString("shopPicture"),
                            shops.getJSONObject(i).getString("price"),
                            shops.getJSONObject(i).getInt("quantity"),
                            shops.getJSONObject(i).getInt("shopId"))
                    );
                }

                ShopArrayAdapter adapter = new ShopArrayAdapter(this, shopList);
                Spinner selectShop = (Spinner)findViewById(R.id.selectShop);
                selectShop.setAdapter(adapter);

                selectShop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {
                        setPosition(position);
                        quantityInShop.setText(String.valueOf(shopList.get(position).getProductQuantity()));
                        productPrice.setText(shopList.get(position).getProductPrice());
                        shopName.setText(shopList.get(position).getName());

                        //setting a new current price
                        setCurrentPrice(Integer.valueOf(shopList.get(position).getProductPrice()));

                        //sets quantity back to 1
                        quantity.setText(String.valueOf(1));

                        //initialise shopId
                        shopId = shopList.get(position).getId();

                    } // to close the onItemSelected
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });
            } // TODO else error message?h
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Whenever a view(in this case Buttons) are clicked.
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        int value = Integer.parseInt(quantity.getText().toString());

        switch (v.getId()) {
            // The "+" button was clicked.
            case R.id.increaseQuantityButton: {
                quantity.setText(String.valueOf(++value));

                //updates price to +1 currentPrice
                int cPrice = getCurrentPrice();
                String stringPrice = String.valueOf(productPrice.getText());
                int price = Integer.valueOf(stringPrice);
                price += cPrice;
                productPrice.setText(String.valueOf(price));
                break;
            }

            // the "-" button was clicked.
            case R.id.decreaseQuantityButton: {
                if (value > 1) {
                   quantity.setText(String.valueOf(--value));

                    //updates price to -1 currentPrice
                    int cPrice = getCurrentPrice();
                    String stringPrice = String.valueOf(productPrice.getText());
                    int price = Integer.valueOf(stringPrice);

                    // makes sure price cant get negative
                    if (price != cPrice) {
                        price -= cPrice;
                    }
                    productPrice.setText(String.valueOf(price));
                }
                break;
            }

            case R.id.add_to_basket: {
                int id;
                id = User.user.getId();
                if (id != -1) {
                    int quanty = Integer.parseInt(quantity.getText().toString());
                    scriptExecutor = new ScriptExecutor("http://folk.ntnu.no/henrihso/addShoppingCart.php?userId=" +id+
                            "&productId=" +productId+ "&shopId=" +shopId+ "&quantity=" +quanty);
                    // Get a reference to the callback response.
                    scriptExecutor.scriptResponse = this;

                    // Run the scriptExecutor.
                    scriptExecutor.execute();

                    Toast toast = Toast.makeText(getApplicationContext(), "Product added to cart",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "User must be logged in to buy products",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
            }
        }
    }

    /**
     * sets the current price for a shop
     * this is not the quantity price
     * this price changes when shop changes1
     * @param currentPrice current price
     */
    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    /**
     * gets the current price for a shop
     * this is not the quantity price
     * this price changes when shop changes
     * @return current Price
     */
    public int getCurrentPrice() {
        return currentPrice;
    }

    /**
     * sets the curent position of the select_shop spinner item that
     * is being used. This means that when user switch shop. the information
     * shown to the user will update
     * @param pos position
     */
    public void setPosition(int pos) {
        this.position = pos;
    }
}
