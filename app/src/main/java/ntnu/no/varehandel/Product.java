package ntnu.no.varehandel;

/**
 * Class used to handle information about products.
 * @author Henrik Haugom Solum
 */

public class Product {
    private String id;
    private String name;
    private String price;
    private String description;
    private String imageURL;

    /**
     * Initializing of the object with the needed information.
     * @param id The id of the product.
     * @param name The name of the product.
     * @param price The price of the product.
     * @param imageURL The path to where the image for this product is located.
     */
    public Product(String id, String name, String price, String imageURL) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imageURL = imageURL;
    }

    /**
     * Returns the id of the product
     * @return id The product id.
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the name of the product.
     * @return name The name of the product.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the price of the product.
     * @return The price of the product.
     */
    public String getPrice() {
        return price;
    }

    /**
     * Gets the image location for a given product.
     * @return The URL to the image for this product.
     */
    public String getImageURL() {
        return imageURL;
    }

    public String getDescription() {
        return description;
    }
}