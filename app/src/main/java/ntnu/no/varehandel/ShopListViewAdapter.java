package ntnu.no.varehandel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adapter for the Expandable ListView in the ShopListViewActivity. Fills the
 * groups and children with the information needed.
 *
 * The code here is inspired by:
 * http://camposha.info/android-custom-expandable-ListView-images-text/
 *
 * @author Henrik Haugom Solum
 */

public class ShopListViewAdapter extends BaseExpandableListAdapter{
    private Context context;
    private ArrayList<Type> types;
    private LayoutInflater inflater;

    /**
     * Initializes the ShopListViewAdapter with the context and the ArrayList to display.
     * @param context The context of the application.
     * @param types The ArrayList with all the different types and their Shop objects.
     */
    public ShopListViewAdapter(Context context, ArrayList<Type> types) {
        this.context = context;
        this.types = types;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Gets the amount of types.
     * @return The size of the types ArrayList.
     */
    @Override
    public int getGroupCount() {
        return types.size();
    }

    /**
     * Returns how many shops a certain type has.
     * @param groupPosition The position of the group.
     * @return The amount of shops under a certain type.
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return types.get(groupPosition).getShopList().size();
    }

    /**
     * Gets the group at the given position.
     * @param groupPosition The position of the group.
     * @return Returns the group at this position.
     */
    @Override
    public Type getGroup(int groupPosition) {
        return types.get(groupPosition);
    }

    /**
     * Gets a single test.
     * @param groupPosition The position of this group in the list view.
     * @param childPosition The position of the child within the group.
     * @return The child in groupPosition -> childPosition
     */
    @Override
    public Shop getChild(int groupPosition, int childPosition) {
        return types.get(groupPosition).getShopList().get(childPosition);
    }

    /**
     * Unused.
     * @param groupPosition Unused.
     * @return Unused.
     */
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    /**
     * Unused.
     * @param groupPosition Unused.
     * @param childPosition Unused.
     * @return Unused.
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    /**
     * Unused.
     * @return Unused.
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Gets the view for the entire group of types.
     * @param groupPosition The position of the group.
     * @param isExpanded Whether this group is expanded or not.
     * @param convertView The view that is going to be converted.
     * @param parent The parent of this view.
     * @return convertView The updated view.
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {

        convertView = inflater.inflate(R.layout.custom_type_item, null);

        // Get the Type item.
        Type type =  getGroup(groupPosition);

        // Set the type name in as the header.
        TextView typeName = (TextView) convertView.findViewById(R.id.typeName);
        typeName.setText(type.getName());

        return convertView;
    }

    /**
     * Gets the view for shops.
     * @param groupPosition The group position of this shop.
     * @param childPosition The child position for this shop.
     * @param isLastChild If this is the last child in the group or not.
     * @param convertView The view that is going to be converted.
     * @param parent The parent of this view.
     * @return convertView The changed view.
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.custom_shop_item, null);

        // Get the information about the shops into their views.
        TextView shopName = (TextView) convertView.findViewById(R.id.shopName);
        ImageView image = (ImageView) convertView.findViewById(R.id.shopPic);

        shopName.setText(getChild(groupPosition, childPosition).getName());

        // Background task to get an image from a web resource.
        new ImageDownloader(image).execute(getChild(groupPosition, childPosition).getImageURL());

        return convertView;
    }

    /**
     * Checks if a child in a certain group is selectable.
     * @param groupPosition The group position the child is in.
     * @param childPosition The child's position in the group.
     * @return true Since we want it to be selectable.
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
