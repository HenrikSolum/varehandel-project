package ntnu.no.varehandel;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Template activity to decide what happens whenever something in the toolbar is clicked, since
 * the toolbar needs to be consistent in all the activities that has a toolbar.
 */
public class TemplateActivity extends AppCompatActivity {
    /**
     * Decides what happens on the creation of the options menu
     * @param menu the menu for the toolbar
     * @return true when option menu has bin created
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        Search search = new Search();
        search.searchProcess(menu, this, getComponentName());

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Maps clicks on menu items to certain specific tasks/method calls.
     * @param item The menu item that was clicked on.
     * @return true or false depending on if a valid menu item was clicked on or not.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                //user pressed the settings item
                Log.i("action_settings", "The settings menu item was clicked.");
                Toast.makeText(getBaseContext(), "There are no settings.", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_search:
                //user pressed the search item
                Log.i("action_search", "The search item was clicked.");
                return true;

            case R.id.action_login:
                Log.i("action_login", "The sign in menu item was clicked.");
                Intent signInIntent = new Intent(getBaseContext(), SignInActivity.class);
                Log.i("INTENT LAUNCH", "SignInActivity was launched.");
                startActivity(signInIntent);
                return true;

            case R.id.action_shopping_cart:
                Intent shoppingCartIntent = new Intent(getBaseContext(), ProductListViewActivity.class);
                shoppingCartIntent.putExtra("title", getString(R.string.shopping_cart));
                shoppingCartIntent.putExtra("scriptUrl",
                        "http://folk.ntnu.no/henrihso/getShoppingCart.php?userId=" + User.user.getId());
                startActivity(shoppingCartIntent);

            default:
                // users action was not recognized
                // Invoke the superclass to handle it.
                Log.w("Unrecognized action", getString(R.string.unrecognized_menu_item));
                return super.onOptionsItemSelected(item);
        }
    }
}
