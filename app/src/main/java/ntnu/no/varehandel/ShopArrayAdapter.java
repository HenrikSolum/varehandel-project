package ntnu.no.varehandel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Custom adapter to manage the custom list view items from custom_row.xml and
 * place them into a product list view.
 * Inspiration: https://www.youtube.com/watch?v=nOdSARCVYic#t=300.286844
 * @author Haakon Reiss-Jacobsen
 */

class ShopArrayAdapter extends ArrayAdapter<Shop> {
    private ArrayList<Shop> shops;
    private int pos;

    /**
     * Stores all information received.
     * @param context The application context.
     */
    ShopArrayAdapter(Context context, ArrayList<Shop> shops) {
        super(context, R.layout.row, shops);

        this.shops = shops;
    }

    /**
     * Creates a custom view for a list view.
     * @param position The position of the item in the list.
     * @param convertView The view that is going to be customized.
     * @param parent The parent node to the list.
     * @return convertView The custom list view as defined in this method.
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }

        ImageView shopImage = (ImageView) convertView.findViewById(R.id.logo);
        TextView shopName = (TextView) convertView.findViewById(R.id.shop_name);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        // Set a default picture while the background task loads the real picture.
        shopImage.setImageResource(R.drawable.default_picture);

        // Set text to match the product.
        shopName.setText(shops.get(position).getName());
        price.setText(shops.get(position).getProductPrice());

        // Background task to get an image from a web resource.
        new ImageDownloader(shopImage).execute(shops.get(position).getImageURL());

        return convertView;
    }

    /**
     * Sets the view for each row when seen in dropdown
     * @param position position in the array
     * @param convertView the view that is going to be customized
     * @param parent the parent node to the list
     * @return the Custom view that has bin defined in this method
     */
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }

        ImageView shopImage = (ImageView) convertView.findViewById(R.id.logo);
        TextView shopName = (TextView) convertView.findViewById(R.id.shop_name);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        shopImage.setImageResource(R.drawable.default_picture);

        // Set text to match the product.
        shopName.setText(shops.get(position).getName());
        price.setText(shops.get(position).getProductPrice());

        // Background task to get an image from a web resource.
        new ImageDownloader(shopImage).execute(shops.get(position).getImageURL());

        //setPos(position);

        return convertView;
    }

    public void setPos (int pos) {
        this.pos = pos;
    }

    public int getPos () {
        return this.pos;
    }
}
