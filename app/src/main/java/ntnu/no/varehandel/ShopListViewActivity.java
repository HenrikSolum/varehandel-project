package ntnu.no.varehandel;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Activity that allows the user to navigate through different types of shops to
 * the single view for a certain shop.
 * @author Henrik Haugom Solum, Jostein Enes
 */
public class ShopListViewActivity extends TemplateActivity implements ScriptResponse {
    private Context context;
    private ExpandableListView simpleExpandableListView;
    ScriptExecutor scriptExecutor = new ScriptExecutor("http://folk.ntnu.no/henrihso/getShops.php");

    // The ArrayList that is going to be filled up with all the types.
    ArrayList<Type> typeList = new ArrayList<>();

    /**
     * On activity launch.
     * @param savedInstanceState Information saved away on destroy.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.shops);
        setContentView(R.layout.activity_shops);

        // Specifying the toolbar layout
        Toolbar myToolbar = (Toolbar) findViewById(R.id.shops_toolbar);
        setSupportActionBar(myToolbar);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.shops_toolbar).setElevation(4);
        }

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        // Get a reference to the expandable ListView.
        simpleExpandableListView = (ExpandableListView) findViewById(R.id.simpleExpandableListView);

        // When a child item is clicked, I.E a shop.
        simpleExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            /**
             * Starts a activity for the given shop.
             * @param parent The parent of this view.
             * @param v The view that was clicked on.
             * @param groupPosition The group position in this view.
             * @param childPosition The child position in the group.
             * @param id The of this view.
             * @return false...
             */
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {

                Shop shop = typeList.get(groupPosition).getShopList().get(childPosition);

                Intent shopIntent = new Intent(getBaseContext(), ShopActivity.class);
                shopIntent.putExtra("id", String.valueOf(shop.getId()));
                shopIntent.putExtra("name", shop.getName());
                shopIntent.putExtra("imageURL", shop.getImageURL());
                startActivity(shopIntent);
                return false;
            }
        });

        // Get a reference to the callback response.
        scriptExecutor.scriptResponse = this;

        // Run the scriptExecutor.
        scriptExecutor.execute();
    }

    /**
     * Callback from the ScriptExecutor. The String s is the JSON that was returned from
     * the php script.  Parses the string (s( as a JSON objects and arrays and fills the view with
     * all the results.
     *
     * @param s The json string to be parsed.
     */
    @Override
    public void finished(String s) {
        try {
            // Handling the JSON object from our API.

            // The different object and array names are decided by the API.
            JSONObject object = new JSONObject(s);
            JSONObject types = object.getJSONObject("types");
            JSONArray type = types.getJSONArray("type");

            for (int i = 0; i < type.length(); i++) {
                // Create a new type and update its information.
                Type typeObj = new Type();
                typeObj.setId(type.getJSONObject(i).getString("id"));
                typeObj.setName(type.getJSONObject(i).getString("name"));

                JSONArray shop = type.getJSONObject(i).getJSONArray("shop");

                // List of individual shops within a type.
                ArrayList<Shop> shopList = new ArrayList<>();

                // Add the id, name and the picturePath to the Shop object.
                for (int j = 0; j < shop.length(); j++) {
                    shopList.add(new Shop(
                            shop.getJSONObject(j).getInt("id"),
                            shop.getJSONObject(j).getString("name"),
                            shop.getJSONObject(j).getString("picturePath")
                    ));
                }
                // Inserts the ArrayList of Shop objects into the Type object.
                typeObj.setShopList(shopList);
                // Adds the Type object into the ArrayList of types.
                typeList.add(typeObj);
            }

            // Set the adapter for the Expandable ListView.
            ShopListViewAdapter adapter = new ShopListViewAdapter(this, typeList);
            simpleExpandableListView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}