package ntnu.no.varehandel;

/**
 * Created by Henrik on 15.11.2016.
 */

public interface ScriptResponse {
    public void finished(String s);
}
