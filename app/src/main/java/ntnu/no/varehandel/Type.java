package ntnu.no.varehandel;

import java.util.ArrayList;

/**
 * Class to maintain information about the different shop types, also
 * keeps an ArrayList of Shop items that belongs to this type.
 * @author Henrik Haugom Solum
 */

public class Type {
    private String id;
    private String name;
    private ArrayList<Shop> shopList;

    /**
     * Gets the id of this type.
     * @return id The id of this type.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id of this type.
     * @param id The id of this type.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the name of this type.
     * @return name The name of this type.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this type.
     * @param name The name of this type.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the ArrayList filled with Shop objects.
     * @return shopList The ArrayList with shop objects.
     */
    public ArrayList<Shop> getShopList() {
        return shopList;
    }

    /**
     * Sets the ArrayList with Shop objects.
     * @param shopList
     */
    public void setShopList(ArrayList<Shop> shopList) {
        this.shopList = shopList;
    }
}
