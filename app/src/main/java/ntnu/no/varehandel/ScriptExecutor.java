package ntnu.no.varehandel;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 * Class to run a script at a specified URL, that must return a string in JSON format. Every
 * listener to the callback of this class(ScriptResponse) will be alerted when the script
 * has been executed.
 *
 * @author Henrik Haugom Solum
 *
 */
    public class ScriptExecutor extends AsyncTask<String, String, String> {
    public ScriptResponse scriptResponse;
    private String scriptURL;
    private String JSONString;

    /**
     * The constructor initialises scriptURL so the ScriptExecutor knows where to find the
     * script that is going to be executed.
     * @param url The URL to the script on the webserver
     */
    public ScriptExecutor(String url) {
        this.scriptURL = url;
    }

    /**
     * Before executing the background task, log what script is about to be executed.
     */
    @Override
    protected void onPreExecute() {
        Log.i("Script URL", this.scriptURL);
    }

    /**
     * Method to get a JSON string from a php script on a webserver.
     * @param params // TODO Describe.
     * @return The JSON
     */
    @Override
    protected String doInBackground(String... params) {
        try {
            // Creates an URL out of the api link
            URL apiUrl = new URL(scriptURL);

            // Opens up a http connection to the URL above
            HttpURLConnection httpURLConnection = (HttpURLConnection) apiUrl.openConnection();

            // Opens up an inputstream to receive data from our URL
            InputStream inputStream = httpURLConnection.getInputStream();

            // A buffer to store the JSON string
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));

            StringBuilder stringBuilder = new StringBuilder();

            // While there's something to fetch from the URL, append it to the stringbuilder
            while ((JSONString = bufferedReader.readLine()) != null) {
                stringBuilder.append(JSONString);
            }

            // Close the resources since they are no longer needed.
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();

            // Return the string, in this case the json.
            return stringBuilder.toString().trim();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    // End of copied code

    /**
     * After the script has been run, this method makes sure to alert the callback of the result.
     * @param s The JSON from the php script.
     */
    @Override
    protected void onPostExecute(String s) {
        scriptResponse.finished(s);
    }
}