package ntnu.no.varehandel;

/**
 * Class to hold user information so that we can access it when needed in the application.
 * @author Henrik Haugom Solum
 */
public class User {
    public static User user  = new User();
    private int id           = -1;
    private String firstName = "Not logged in.";
    private String lastName  = "Not logged in.";

    /**
     * Private constructor so nobody can make objects of this class, as one application will only
     * deal with one user at a time.
     */
    private User() {}

    /**
     * Gets the id of the logged in user.
     * @return id The id of the user.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the logged in user.
     * @param id The id of the user.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the first name of the user.
     * @return firstName The name of the user.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the user.
     * @param firstName The first name of the user.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name of the user.
     * @return lastName The last name of the user.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the user.
     * @param lastName The last name of the user.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the full name of the logged in user.
     * @return The full name of the logged in user.
     */
    public String getName() {
        return getFirstName() + " " + getLastName();
    }
}
