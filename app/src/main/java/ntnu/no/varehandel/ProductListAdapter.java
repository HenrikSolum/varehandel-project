package ntnu.no.varehandel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Custom adapter to manage the custom list view items from custom_row.xml and
 * place them into a product list view.
 * Inspiration: https://www.youtube.com/watch?v=nOdSARCVYic#t=300.286844
 * @author Henrik Haugom Solum
 */

class ProductListAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> products;

    /**
     * Stores all information received.
     * @param context The applicatio context.
     */
    ProductListAdapter(Context context, ArrayList<Product> products) {
        super(context, R.layout.custom_row, products);

        this.products = products;
    }

    /**
     * Creates a custom view for a list view.
     * @param position The position of the item in the list.
     * @param convertView The view that is going to be customized.
     * @param parent The parent node to the list.
     * @return convertView The custom list view as defined in this method.
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater  inflater = LayoutInflater.from(getContext());

        // Only inflate if there's nothing in the convertView.
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.custom_row, parent, false);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.productImage);
        TextView id = (TextView) convertView.findViewById(R.id.productID);
        TextView name = (TextView) convertView.findViewById(R.id.productName);
        TextView price = (TextView) convertView.findViewById(R.id.productPrice);

        // Set a default picture while the background task loads the real picture.
        image.setImageResource(R.drawable.default_picture);

        // Set text to match the product.
        id.setText(products.get(position).getId());
        name.setText(products.get(position).getName());
        price.setText(products.get(position).getPrice());

        // Background task to get an image from a web resource.
        new ImageDownloader(image).execute(products.get(position).getImageURL());

        return convertView;
    }
}