package ntnu.no.varehandel;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Class to show the user a product list view. Either all the products in a store,
 * category, etc or based on what the user entered in the search field.
 *
 * @author Henrik Haugom Solum
 */
public class ProductListViewActivity extends TemplateActivity implements ScriptResponse {
    private Context context;
    private ScriptExecutor scriptExecutor;
    private ArrayList<Product> productList;

    private TextView feedback;
    private String title;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        title = getIntent().getStringExtra("title");
        setTitle(title);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_view);

        // Specifying the toolbar layout
        Toolbar myToolbar = (Toolbar) findViewById(R.id.product_toolbar);
        setSupportActionBar(myToolbar);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.product_toolbar).setElevation(4);
        }

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        String scriptUrl = getIntent().getStringExtra("scriptUrl");
        scriptExecutor = new ScriptExecutor(scriptUrl);

        // Initializing variables.
        this.context = this;
        productList = new ArrayList<>();

        // Get a reference to the callback response.
        scriptExecutor.scriptResponse = this;

        // Run the scriptExecutor.
        scriptExecutor.execute();

        feedback = (TextView) findViewById(R.id.feedback);
        feedback.setText(title);
    }

    /**
     * Callback from the ScriptExecutor. The String s is the JSON that was returned from
     * the php script.  Parses the string (s( as a JSON objects and arrays and fills the view with
     * all the results.
     * @param s The json string to be parsed.
     */
    @Override
    public void finished(String s) {
        try {
            // Handling the JSON object from our API.
            JSONObject json = new JSONObject(s);

            if(!(json.getInt("code") == 404)) {
                JSONArray products = json.getJSONObject("products").getJSONArray("product");
                int amount = products.length();

                // Adds 's' to 'result' if there are more than one result.
                String plural = (amount > 1 || amount == 0) ? "s" : "";
                feedback.setText(String.format("Found %s result%s: ", amount, plural));

                // Get all the information from the JSON and add them to a Product object.
                for (int i = 0; i < amount; i++) {
                    productList.add(new Product(
                            products.getJSONObject(i).getString("id"),
                            products.getJSONObject(i).getString("name"),
                            products.getJSONObject(i).getString("price"),
                            products.getJSONObject(i).getString("picture"))
                    );
                }

                /**
                 * Create a list view with the information from the JSON and add listeners to
                 * every item.
                 */
                ListAdapter listAdapter = new ProductListAdapter(this, productList);
                ListView listView = (ListView) findViewById(R.id.listView);
                listView.setAdapter(listAdapter);

                listView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.i("Activity change", "ProductActivity LAUNCHED!");

                                Product product = (Product) parent.getAdapter().getItem(position);

                                Intent productsIntent = new Intent(getBaseContext(), ProductActivity.class);
                                productsIntent.putExtra("id", product.getId());
                                startActivity(productsIntent);
                            }
                        }
                );
            } else {
                if(title.startsWith("Shopping") && (User.user.getId() == -1)) {
                    feedback.setText(R.string.log_in_shopping_cart);
                } else feedback.setText(R.string.empty_cart);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
