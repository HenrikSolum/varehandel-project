package ntnu.no.varehandel;

import android.content.ComponentName;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.SearchView;
import android.view.Menu;
/**
 * This class handles searching for products in the application.
 *
 * This class is inspired by the following stackoverflow thread.
 * http://stackoverflow.com/questions/19588311/how-to-get-the-text-entered-in-search-in-action-bar-to-a-string
 *
 * @author Haakon Reiss-Jacobsen, Henrik Haugom Solum
 */
class Search {
    /**
     * Initializes the search bar and different listeners for the search fields.
     * The following code is inspired by
     * http://stackoverflow.com/questions/19588311/how-to-get-the-text-entered-in-search-in-action-bar-to-a-string
     * @param menu A reference to the menu that the searchbar is placed in.
     * @param context Reference to the context of the activity that initiated the search.
     * @param cName The name of the component that initiated the search.
     */
    void searchProcess (Menu menu, final Context context, ComponentName cName) {

        SearchManager searchManager = (SearchManager) context.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(cName));
            searchView.setIconifiedByDefault(false);
        }

        /*
         * Listener for the search fields.
         * The following code is inspired by
         * http://stackoverflow.com/questions/19588311/how-to-get-the-text-entered-in-search-in-action-bar-to-a-string
         */

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {

            /**
             * Called whenever there's been a change in the input for the search.
             * @param newText The contents of the search field.
             * @return state
             */
            public boolean onQueryTextChange(String newText) {
                /*
                 * If we get time to implement this.
                 */
                return true;
            }

            /**
             * Called whenever a user submits a search query. This method is not concerned with
             * if the query returns nothing from the database or not, that is handled in the
             * activity that displays the results(in this case ProductListViewActivity).
             * @param query The query containing the string entered by the user.
             * @return True or false depending on if something was entered or not.
             */
            public boolean onQueryTextSubmit(String query) {
                if(query != null && !query.isEmpty()) {
                    Intent productsIntent = new Intent(context, ProductListViewActivity.class);
                    productsIntent.putExtra("scriptUrl", "http://folk.ntnu.no/henrihso/search.php?name=" + query);
                    productsIntent.putExtra("title", context.getString(R.string.products));
                    context.startActivity(productsIntent);
                    return true;
                } else return false;
            }
        };

        if (searchView != null) {
            searchView.setOnQueryTextListener(queryTextListener);
        }
    }
}
