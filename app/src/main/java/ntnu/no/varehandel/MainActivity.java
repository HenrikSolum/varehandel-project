package ntnu.no.varehandel;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * The main activity for this application. The first thing that the user sees when opening
 * the application, which contains buttons and toolbars for navigation throughout the app.
 */
public class MainActivity extends TemplateActivity implements View.OnClickListener {
    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.main_list_title);
        setContentView(R.layout.activity_main);

        // Specifying the toolbar layout
        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.main_toolbar).setElevation(4);
        }

        // Initiates the buttons in the activity
        Button productListViewBtn = (Button) findViewById(R.id.productsBtn);
        Button shopListViewBtn = (Button) findViewById(R.id.shopsBtn);
        Button favoritesBtn = (Button) findViewById(R.id.favoritesBtn);
        Button discountListViewBtn = (Button) findViewById(R.id.discountsBtn);


        // Adding listeners to the buttons
        productListViewBtn.setOnClickListener(this);
        shopListViewBtn.setOnClickListener(this);
        favoritesBtn.setOnClickListener(this);
        discountListViewBtn.setOnClickListener(this);
    }

    /**
     * When something clickable has been clicked. Decides what to do based on what view was clicked.
     * @param v The view that was clicked(example: Button).
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.productsBtn:
                Intent productsIntent = new Intent(getBaseContext(), ProductListViewActivity.class);
                productsIntent.putExtra("scriptUrl", "http://folk.ntnu.no/henrihso/search.php");
                productsIntent.putExtra("title", getString(R.string.products));
                startActivity(productsIntent);
                break;

            case R.id.shopsBtn:
                Intent shopsIntent = new Intent(getBaseContext(), ShopListViewActivity.class);
                startActivity(shopsIntent);
                break;

            case R.id.discountsBtn:
                Toast.makeText(getBaseContext(), R.string.not_implemented, Toast.LENGTH_SHORT).show();
                break;

            case R.id.favoritesBtn:
                Intent favoriteIntent = new Intent(getBaseContext(), ProductListViewActivity.class);
                favoriteIntent.putExtra("scriptUrl", "http://folk.ntnu.no/henrihso/getFavorites.php?userId=" + User.user.getId());
                favoriteIntent.putExtra("title", getString(R.string.favorites));
                startActivity(favoriteIntent);
                break;

            default:
                break;
        }
    }
}


