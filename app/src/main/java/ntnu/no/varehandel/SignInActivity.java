package ntnu.no.varehandel;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Activity to demonstrate basic retrieval of the Google user's ID, email address, and basic
 * profile.
 */
public class SignInActivity extends TemplateActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener,
        ScriptResponse {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private TextView title;
    private SignInButton signInButton;
    private Button signOutButton;
    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.login);
        setContentView(R.layout.activity_sign_in);

        // Specifying the toolbar layout
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        // Sets elevation to 4 for the toolbar, if android api is 16 or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.toolbar).setElevation(4);
        }
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);
        // Views
        // mStatusTextView = (TextView) findViewById(R.id.status);
        title = (TextView) findViewById(R.id.title);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signOutButton = (Button) findViewById(R.id.sign_out_button);

        signInButton.setOnClickListener(this);
        signOutButton.setOnClickListener(this);

        // If a user is already logged in, update the view.
        if(User.user.getId() > -1) {
            title.setText(String.format(getString(R.string.hello_user), User.user.getName()));
            signInButton.setEnabled(false);
            signOutButton.setEnabled(true);
        }

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .requestEmail()
                .requestProfile()
                //.requestIdToken(CLIENT_ID)
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    /**
     * Handles the result from the GoogleSignIn intent launch.
     * @param requestCode The status code returned from the intent.
     * @param resultCode
     * @param data The data returned from the intent.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    /**
     * Handles the result from the sign in. The result has a reference to
     * the Google account that has additional information about the user.
     * @param result The GoogleSignIn data.
     */
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            title.setText(String.format(getString(R.string.hello_user), account.getDisplayName()));
            signInButton.setEnabled(false);
            signOutButton.setEnabled(true);

            /*
             * Tries to insert a user into the database using his/hers Google credentials.
             */
            String url = "http://folk.ntnu.no/henrihso/createUser.php" +
                    "?gid="   + account.getId() +
                    "&email=" + account.getEmail() +
                    "&fn="    + account.getGivenName() +
                    "&ln="    + account.getFamilyName();

            ScriptExecutor scriptExecutor = new ScriptExecutor(url);

            // Get a reference to the callback response.
            scriptExecutor.scriptResponse = this;

            // Run the scriptExecutor.
            scriptExecutor.execute();
        } else {
            // Signed out, show unauthenticated UI.
            Log.e("handleSignInResult", "Could not get user data from Google.");
        }
    }

    /**
     * Handles the different buttons and map them to their correct actions.
     * @param v The button that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
        }
    }

    /**
     * Handles what happens if the connection to the Google Sign In fails.
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Connection failed", connectionResult.toString());
    }

    /**
     * Starts an activity that allows the user to Sign in through a Google User.
     */
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Signs the user out.
     */
    private void signOut() {
        User.user.setId(-1);
        User.user.setFirstName(getString(R.string.not_logged_in));
        User.user.setLastName(getString(R.string.not_logged_in));

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        title.setText(R.string.sign_in);
                        signInButton.setEnabled(true);
                        signOutButton.setEnabled(false);
                    }
                }
        );
    }

    /**
     * Updates the one user object the application has to match the logged in user.
     * @param id The id of the logged in user.
     * @param firstName The firstName of the logged in user.
     * @param lastName The lastName of the logged in user.
     */
    private void updateUser(int id, String firstName, String lastName) {
        if(id > -1 && firstName != null && lastName != null){
            User.user.setId(id);
            User.user.setFirstName(firstName);
            User.user.setLastName(lastName);
        }
    }

    /**
     * Response from the API after trying to create a user with google credentials.
     * @param s The json with the status code.
     */
    @Override
    public void finished(String s) {
        try {
            // Handling the JSON object from our API.
            JSONObject json = new JSONObject(s);

            if ((json.getInt("code") == 200)) { // New user created.
                Log.i("Success", "User inserted to database.");
                updateUser(json.getInt("id"), json.getString("firstName"), json.getString("lastName"));
            } else if(json.getInt("code") == 201) { // User existed or something went wrong.
                Log.i("Info", "User exists, id = " + json.getInt("id"));
                updateUser(json.getInt("id"), json.getString("firstName"), json.getString("lastName"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}